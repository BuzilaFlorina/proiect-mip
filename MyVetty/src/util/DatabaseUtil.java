package util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import model.Animal;
import model.Appointment;
import model.MedicalPersonnel;

public class DatabaseUtil {
	public static EntityManagerFactory entityManagerFactory; // se ocupa de conexiune
	public static EntityManager entityManager; // se ocupa de tranzactii

	public void setUp() throws Exception {
		entityManagerFactory = Persistence.createEntityManagerFactory("MyVetty");
		entityManager = entityManagerFactory.createEntityManager();
	}

	/**
	 * use this function to save an animal in the database
	 * @param animalX is the animal we want to save
	 */
	public void saveAnimal(Animal animalX) {
		entityManager.persist(animalX);
	}

	/**
	 * this is a function to save a medical personnel in the database
	 * @param medic is the medical personnel we want to save
	 */
	public void saveMedic(MedicalPersonnel medic) {
		entityManager.persist(medic);
	}

	/**
	 * this is a function to save an appointment in the database
	 * @param appointment is the appointment we will save
	 */
	public void saveAppointment(Appointment appointment) {
		entityManager.persist(appointment);
	}

	public void startTransaction() {
		entityManager.getTransaction().begin();
	}

	public void commitTransaction() {
		entityManager.getTransaction().commit();
	}

	/**
	 * this function closes the connection with the database
	 */
	public void closeEntityManager() {
		entityManager.close();
	}

	/**
	 * this function prints all animals from the database
	 */
	public void printAllAnimalsFromDB() {
		List resultList = entityManager.createNativeQuery("Select * From  my_vetty.animal", Animal.class)
				.getResultList();
		List<Animal> results = resultList;
		for (Animal animalX : results)
			System.out.println("Animal : " + animalX.getName() + " has ID : " + animalX.getIdanimal());
	}

	/**
	 * this function prints all medicals personnel from the database
	 */
	public void printAllMedicsFromDB() {
		List<MedicalPersonnel> results = entityManager
				.createNativeQuery("Select * From my_vetty.medical_personnel", MedicalPersonnel.class).getResultList();
		for (MedicalPersonnel medic : results)
			System.out.println("Medic : " + medic.getFirstname() + " has ID : " + medic.getIdPersonnel());
	}

	/**
	 * this function prints all appointments from the database
	 */
	public void printAllAppointmentsFromDB() {
		List<Appointment> results = entityManager
				.createNativeQuery("Select * From my_vetty.Appointment", Appointment.class).getResultList();
		for (Appointment appointment : results)
			System.out.println("The Animal " + appointment.getAnimalBean().getName() + " with the ID : "
					+ appointment.getAnimalBean().getIdanimal() + " has an appointment with "
					+ appointment.getMedicalPersonnel().getFirstname() + " "
					+ appointment.getMedicalPersonnel().getLastname() + "on " + appointment.getDate());
	}

	/**
	 * this function searches for the animal in the database
	 * @param id is the identifier of the animal we are looking for
	 * @return
	 */
	public Animal findAnimal(int id) {
		return entityManager.find(Animal.class, id);
	}

	/**
	 * this function removes an animal from the database, after the given id is found
	 * @param id is the identifier of the animal we want to remove
	 */
	public void removeAnimal(int id) {
		Animal animal = findAnimal(id);
		if (animal != null) {
			entityManager.remove(animal);
		}
	}

	/**
	 * this function searches for the medical personnel in the database
	 * @param id is the identifier of the medical personnel we are looking for
	 * @return
	 */
	public MedicalPersonnel findDoctor(int id) {
		return entityManager.find(MedicalPersonnel.class, id);
	}

	/**
	 * this function removes a medical personnel from the database, after the given id is found
	 * @param id is the identifier of the medical personnel we want to remove
	 */
	public void removeMedicalPersonnel(int id) {
		MedicalPersonnel medic = findDoctor(id);
		if (medic != null) {
			entityManager.remove(medic);
		}
	}

	/**
	 * this function searches for the appointment in the database
	 * @param id is the identifier of the appointment we are looking for
	 * @return
	 */
	public Appointment findAppointment(int id) {
		return entityManager.find(Appointment.class, id);
	}

	/**
	 * this function removes an appointment from the database, after the given id is found
	 * @param id is the identifier of the appointment we want to remove
	 */
	public void removeAppointment(int id) {
		Appointment app = findAppointment(id);
		if (app != null) {
			entityManager.remove(app);
		}
	}

/**
 * this function is used to update the dates of an animal from the database
 * @param id is the idetifier of the animal
 * @param name is the name of the animal
 * @param species is the species of the animal
 * @param birth is the date of birth of the animal
 * @param bef says if the animal had any interventions before
 * @param weight is the weight of the animal (in kg)
 * @param ownerPhone is the phone number of the owner
 * @param ownerName is the owner name
 * @param ownerEmail is the owner email
 * @throws ParseException
 */

	public void updateAnimal(int id, String name, String species, String birth, byte bef, int weight, String ownerPhone, String ownerName, String ownerEmail) throws ParseException {
		Animal anim = findAnimal(id);
		if (anim != null) {
			anim.setName(name);
			anim.setSpecies(species);
			anim.setPreInterventions(bef);
			Date dataB = new SimpleDateFormat("dd/MM/yyyy").parse(birth);
			anim.setDateOfBirth(dataB);
			anim.setOwnerEmail(ownerEmail);
			anim.setWeight(weight);
			anim.setOwnerName(ownerName);
			anim.setOwnerPhone(ownerPhone);
			
			System.out.println("Animal updated!");
		} else
			System.out.println("We can't find your animal");
	}

	/**
	 * this function updates the date of an appointment form the database
	 * @param id is the identifier of the appointment we want to update
	 * @param date is the date that will change
	 * @throws ParseException
	 */
	public void updateAppointment(int id, String date) throws ParseException {
		//first i searched for the id of the appointment
		Appointment app = findAppointment(id);
		if (app != null) {
			Date data = new SimpleDateFormat("dd/MM/yyyy").parse(date);
			app.setDate(data);
			System.out.println("Appointment updated!");
		} else
			System.out.println("We can't find your appointment!");
	}
	
	/**
	 * this function updates the date of an appointment form the database
	 * @param id is the identifier of the appoitnment we want to update
	 * @param date is the date of the new appointment
	 * @param animal is the animal
	 * @param doctor is the medical pesonnel
	 */
	public void updateAppointmentForList(int id, Date date, Animal animal, MedicalPersonnel doctor) throws ParseException {
		//first i searched for the id of the appointment
		Appointment app = findAppointment(id);
		if (app != null) {
			app.setDate(date);
			app.setAnimalBean(animal);
			app.setMedicalPersonnel(doctor);
			System.out.println("Appointment updated!");
		} else
			System.out.println("We can't find your appointment!");
	}


	/**
	 * this function changes the medical personnel with the given parameters
	 * @param id is the id of the medical personnel that we are going to change the dates
	 * @param firstName is the first name of the medical personnel
	 * @param lastName is the last name of the medical personnel
	 * @param age is the age of the medical personnel
	 * @param startDate is the date when the medical personnel started working
	 * @param endDate is the date when the medical personnel stopped working
	 * @throws ParseException
	 */
	public void updateMedicalPersonnel(int id, String firstName, String lastName, int age, String startDate, String endDate) throws ParseException {
		//first i searched for the medical personnel with the given id
		MedicalPersonnel medic = findDoctor(id);
		if (medic != null) {
			medic.setFirstname(firstName);
			medic.setLastname(lastName);
			medic.setAge(age);
			Date dataS = new SimpleDateFormat("dd/MM/yyyy").parse(startDate);
			medic.setStartDate(dataS);
			Date dataE = new SimpleDateFormat("dd/MM/yyyy").parse(endDate);
			medic.setEndDate(dataE);
			System.out.println("Medic updated!");
		} else
			System.out.println("We can't find the medic !");
	}

	/**
	 * returns a list of the animals from the database
	 * @return
	 */
	public List<Animal> animalList() {
		List<Animal> results = entityManager.createNativeQuery("Select * From  my_vetty.animal", Animal.class)
				.getResultList();

		return results;
	}

	/**
	 * returns a list of all appointments in the database
	 * @return
	 */
	public List<Appointment> appList() {
		List<Appointment> results = entityManager
				.createNativeQuery("Select * From  my_vetty.appointment", Appointment.class).getResultList();
		return results;
	}

	/**
	 * returns a list of all the animals with the species dog in the database
	 * @return
	 */
	public List<Animal> dogList() {
		List<Animal> results = entityManager
				.createNativeQuery("Select * From  my_vetty.animal where species =\"dog\"; ", Animal.class)
				.getResultList();

		return results;
	}

	/**
	 * returns a list of all the animals with the species cat in the database
	 * @return
	 */
	public List<Animal> catList() {
		List<Animal> results = entityManager
				.createNativeQuery("Select * From  my_vetty.animal where species =\"cat\"; ", Animal.class)
				.getResultList();

		return results;
	}

}
