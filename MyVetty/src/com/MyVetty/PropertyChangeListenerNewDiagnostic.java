package com.MyVetty;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

public class PropertyChangeListenerNewDiagnostic {
	
	public String diagnostic;
	public PropertyChangeSupport support;
	
	public PropertyChangeListenerNewDiagnostic() {
		support = new PropertyChangeSupport(this);
	}

	public void addPropertyChangeListener(PropertyChangeListener pcl) {
		support.addPropertyChangeListener(pcl);
	}

	public void removePropertyChangeListener(PropertyChangeListener pcl) {
		support.removePropertyChangeListener(pcl);
	}

	public String getDiagnostic() {
		return diagnostic;
	}

	public void setDiagnostic(String diagnostic) {
		support.firePropertyChange(diagnostic, this.diagnostic, diagnostic);
		this.diagnostic = diagnostic;
	}
}
