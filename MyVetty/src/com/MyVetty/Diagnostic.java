package com.MyVetty;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public class Diagnostic implements PropertyChangeListener {
	
	private String id;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public void propertyChange(PropertyChangeEvent event) {
		this.setId((String) event.getNewValue());
		
	}
}