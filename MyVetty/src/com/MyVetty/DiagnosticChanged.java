package com.MyVetty;

public interface DiagnosticChanged {
	public void update(Object o);
}
