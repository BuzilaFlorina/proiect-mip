package testss;

import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.*;

import org.eclipse.persistence.jpa.jpql.Assert;
import org.junit.jupiter.api.Test;

import generic.Mammal;
import generic.Owner;

class GenericTests {

	/**
	 * test for the constructor of an Owner
	 * this should create a new owner
	 */
	@Test
	void ownerConstructorWithParametersTest() {
		Owner ow = new Owner("Alin", 2, 6);
		if(!(ow.getName().equals("Alin") && ow.getNumAppDone() == 2 && ow.getNumAppointments() == 6))
			fail();
	}
	
	/**
	 * this is a test of the function addMammal method
	 * i created a new mammal and added to a list
	 */
	@Test
	void addMammalTest() {
		
		Mammal ma = new Mammal("Valeria");
		Owner ow = new Owner("Alin", 2, 6);
		ow.addMammal(ma);
		if(ow.numAnimals() == 0)
			fail();
	}
	
	/**
	 * test for treatedAnimals method
	 * when called this function should increase the number of treated animals of an owner or just to announce the owner that his appointments are done
	 */
	@Test
	void treatedAnimalsTest() {
		
		Owner o1 = new Owner("Marin", 3, 3);
		Owner o2 = new Owner ("Nicu", 4, 7);
		
		o1.treatedAnimals();
		Assert.isEqual(o1.getNrTreatedAnimals(), 1, "Not Good");
		o2.treatedAnimals();
		Assert.isEqual(o2.getNumAppDone(), 5, "nooo");
	}
	
	/**
	 * test for compareTo method
	 * this should return -1 because the first owner o1 has less appointments than o2
	 */
	@Test
	void compareToTest() {
		Owner o1 = new Owner("Owner1", 3, 3);
		Owner o2 = new Owner ("Owner2", 4, 7);
		
		if(o1.compareTo(o2) == -1)
			fail();
	}
	
}
