package listener;

import model.Animal;


public class CheckName implements Runnable {

	private ActionPerformed actionPerformed;

	public void setActionPerformed(ActionPerformed actionPerformed) {
		this.actionPerformed = actionPerformed;
	}

	@Override
	public void run() {
		Animal animal = new Animal();
		
		animal.setName("Lolo");

		String result;
		Exception es = null;
		try {
			result = animal.getName();
			if(result == null)
				throw es;
			actionPerformed.actionPerformed();
		} catch (Exception e) {
			System.out.println("The name of the animal with the id " + animal.getIdanimal() + " is null !");
		}

	}

}