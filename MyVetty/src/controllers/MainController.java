package controllers;

import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import javax.imageio.ImageIO;

import com.sun.javafx.image.impl.ByteIndexed.ToByteBgraAnyConverter;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import model.Animal;
import model.Appointment;
import singleton.Diagnostic;
import singleton.ToDoAnimal;
import util.DatabaseUtil;

public class MainController implements Initializable {

	private List<Diagnostic> diagnostics;

	@FXML
	private TextArea txt_prescription;

	@FXML
	private TextArea txt_diagnostic;

	@FXML
	private ListView<String> app_today;

	@FXML
	private Button btn_send;

	@FXML
	private Label lbl_name;

	@FXML
	private Label lbl_titleNameAnimal;

	@FXML
	private Label lbl_species;

	@FXML
	private Label lbl_titleSpeciesAnimal;

	@FXML
	private Label lbl_weight;

	@FXML
	private Label lbl_titleWeightAnimal;

	@FXML
	private Label lbl_preIntervention;

	@FXML
	private Label lbl_titleInterventionsAnimal;

	@FXML
	private Label lbl_nameOwner;

	@FXML
	private Label lbl_titleOwnerName;

	@FXML
	private Label lbl_phoneOwner;

	@FXML
	private Label lbl_titleOwnerPhone;

	@FXML
	private Label lbl_emailOwner;

	@FXML
	private Label lbl_titleOwnerEmail;

	@FXML
	private Label lbl_owner;

	@FXML
	private Label lbl_animal;

	@FXML
	private MenuBar meniuBar;

	@FXML
	private Menu mnu_edit;

	@FXML
	private Menu mnu_add;

	@FXML
	private MenuItem mnu_addApp;

	@FXML
	private MenuItem mnu_addAnimal;

	@FXML
	private MenuItem mnu_addMedic;

	@FXML
	private Menu mnu_remove;

	@FXML
	private MenuItem mnu_removeApp;

	@FXML
	private MenuItem mnu_removeAnimal;

	@FXML
	private MenuItem mnu_removeMedic;

	@FXML
	private Menu mnu_update;

	@FXML
	private MenuItem mnu_updateApp;

	@FXML
	private MenuItem mnu_updateAnimal;

	@FXML
	private MenuItem mnu_updateMedic;

	@FXML
	private ImageView img_poza;

	/**
	 * this function returns a list with the names of the animals
	 * 
	 * @param animals is a list with animals
	 * @return
	 */
	public ObservableList<String> getAnimalName(List<Animal> animals) {

		ObservableList<String> names = FXCollections.observableArrayList();
		for (Animal a : animals) {
			names.add(a.getName());
		}

		return names;
	}

	/**
	 * this function returns a list with the dates of the appointments
	 * 
	 * @param apps is a list of appointments
	 * @return
	 */
	public ObservableList<String> getAppointments(List<Appointment> apps) {

		ObservableList<String> result = FXCollections.observableArrayList();
		for (Appointment a : apps) {
			result.add(String.valueOf(a.getDate()));
		}

		return result;
	}

	/**
	 * a function to populate the list view in the left side a list of sorted by
	 * date appointments will be shown in the right side the dates of the animals
	 * and their owners will be shown
	 * 
	 * @throws Exception
	 */
	public void populateMainListView() throws Exception {

		DatabaseUtil db = new DatabaseUtil();
		db.setUp();
		db.startTransaction();

		
		Appointment app = new Appointment();
		List<Appointment> apps = db.appList();
		List<Appointment> appDbList = app.sortedApp(db);
		for(Appointment ap : apps){
			for(Appointment aap : appDbList) {
				if(ap.getIdappointment() == app.getIdappointment())
				db.updateAppointmentForList(ap.getIdappointment(), aap.getDate(), aap.getAnimalBean(), aap.getMedicalPersonnel());
			}
		}
		
		ObservableList<String> appDatesList = getAppointments(appDbList);

		app_today.setItems(appDatesList);
		app_today.refresh();

		app_today.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
			public void changed(ObservableValue<? extends String> ov, String old_val, String new_val) {
				if (new_val != null) {
					for (Appointment app : appDbList) {
						if (String.valueOf(app.getDate()).equals(new_val)) {
							lbl_animal.setText("ANIMAL");
							lbl_titleNameAnimal.setText("NAME:");
							lbl_titleWeightAnimal.setText("WEIGHT:");
							lbl_titleSpeciesAnimal.setText("SPECIES:");
							lbl_titleInterventionsAnimal.setText("PREINTERVENTIONS:");
							lbl_name.setText(app.getAnimalBean().getName());
							lbl_species.setText(app.getAnimalBean().getSpecies());
							lbl_weight.setText(String.valueOf(app.getAnimalBean().getWeight()));
							lbl_owner.setText("OWNER");
							lbl_titleOwnerName.setText("NAME:");
							lbl_titleOwnerPhone.setText("PHONE:");
							lbl_titleOwnerEmail.setText("E-MAIL:");
							lbl_nameOwner.setText(app.getAnimalBean().getOwnerName());
							lbl_phoneOwner.setText(String.valueOf(app.getAnimalBean().getOwnerPhone()));
							lbl_emailOwner.setText(String.valueOf(app.getAnimalBean().getOwnerEmail()));
							BufferedImage img = null;
							try {
								img = ImageIO.read(new ByteArrayInputStream(app.getAnimalBean().getAnimalPicture()));
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							img_poza.setImage(SwingFXUtils.toFXImage(img, null));
							if (app.getAnimalBean().getPreInterventions() == 1) {
								lbl_preIntervention.setText("YES");
								txt_prescription.setText(getPrescription(appDbList));
							} else {
								lbl_preIntervention.setText("NO");
								txt_prescription.setText(" ");
							}
						}
					}
				}
			}
		});

		db.closeEntityManager();
	}

	/**
	 * a function which returns a list of appointments from the database
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<Appointment> Apps() throws Exception {
		DatabaseUtil db = new DatabaseUtil();
		db.setUp();
		db.startTransaction();
		List<Appointment> apps = db.appList();

		db.closeEntityManager();
		return apps;
	}
	
	/**
	 * a function to remove an appointment from the database
	 * @param app the appointment we want to remove
	 * @throws Exception
	 */
	public void removeApp(Appointment app) throws Exception {
		DatabaseUtil db = new DatabaseUtil();
		db.setUp();
		db.startTransaction();
		List<Appointment> apps = db.appList();
		for(Appointment appss : apps) {
			if(appss.getIdappointment() == app.getIdappointment()) {
				db.removeAppointment(app.getIdappointment());
			}
		}
		db.commitTransaction();
		db.closeEntityManager();
	}

	/**
	 * returns the description of the prescription of an animal
	 * 
	 * @param apps is a list of the appointments from the data base.
	 * @return
	 */
	public String getPrescription(List<Appointment> apps) {
		for (Diagnostic pre : diagnostics) {
			for (Appointment app : apps) {
				if (pre.getIdAnimal().equals(String.valueOf(app.getAnimalBean().getIdanimal()))) {
					return pre.getDescription();
				}
			}
		}
		return " ";
	}

	/**
	 * this function makes the button "Send" when is clicked to print in file
	 * "Diagnostic.txt" whatever is written in the text area and the id of the
	 * animal with the date of appointment clicked in that moment
	 * 
	 * @param event
	 * @throws IOException
	 */
	public void Send(ActionEvent event) throws IOException {
		BufferedWriter bw = new BufferedWriter(new FileWriter("Diagnostic.txt", true));
		int idAnimal = -1;
		if (event.getSource() == btn_send) {
			app_today.getSelectionModel().getSelectedItem();
			try {
				List<Appointment> apps = Apps();
				for (Appointment a : apps)
					if (String.valueOf(a.getDate()).equals(app_today.getSelectionModel().getSelectedItem())) {
						idAnimal = a.getAnimalBean().getIdanimal();
						removeApp(a);
						populateMainListView();
					}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			bw.newLine();
			bw.write(txt_diagnostic.getText() + "\t" + String.valueOf(idAnimal));
			txt_diagnostic.setText("");
		}
		bw.close();
	}
	

	public void AddAnimal(ActionEvent event) throws IOException {
		if (event.getSource() == mnu_addAnimal) {
			Stage primaryStage = new Stage();
			BorderPane root = FXMLLoader.load(getClass().getResource("/controllers/createAnimal.fxml"));
			Scene scene = new Scene(root, 800, 800);
			primaryStage.setScene(scene);
			primaryStage.show();
		}

	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		try {

			ToDoAnimal.getInstance().loadPrescriptions();

			diagnostics = ToDoAnimal.getInstance().getPrescription();

			populateMainListView();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
