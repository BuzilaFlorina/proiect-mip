package controllers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class LoginController {
	
	@FXML
	private Label lblStatus;

	@FXML
	private TextField txtUsername;

	@FXML
	private TextField txtPassword;

	/**
	 * this is the function for the login window
	 * if the username and the password are introduced correct a new window will be shown
	 * the values of the username and the password are sent to a server to be checked
	 * @param event is when the button "Login" is clicked
	 * @throws Exception
	 */
	public void Login(ActionEvent event) throws Exception {
		try {
			Socket socket = new Socket("localhost", 5000);
			BufferedReader echoes = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			
			PrintWriter stringToEcho = new PrintWriter(socket.getOutputStream(), true);
			
			String echoString;
			String echoString2;
			String response;
			
			echoString = txtUsername.getText();
			echoString2 = txtPassword.getText();
			stringToEcho.println(echoString);
			stringToEcho.println(echoString2);
			
			response = echoes.readLine();
	
			if(response.equals("da"))
			{
				lblStatus.setText("Login succes");
				Stage primaryStage = new Stage();
				BorderPane root = (BorderPane) FXMLLoader.load(getClass().getResource("/controllers/MainView.fxml"));
				Scene scene = new Scene(root, 800, 800);
				primaryStage.close();
				primaryStage.setScene(scene);
				primaryStage.show();
			} else {
				lblStatus.setText("Login failed !");
			}
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
