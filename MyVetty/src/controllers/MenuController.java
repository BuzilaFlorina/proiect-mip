package controllers;

import java.text.SimpleDateFormat;
import java.util.Date;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import model.Animal;

public class MenuController {
		@FXML
	    private TextField txt_nameA;

	    @FXML
	    private TextField txt_dateBirth;

	    @FXML
	    private Button btb_add;

	    @FXML
	    private TextField txt_species;

	    @FXML
	    private TextField txt_weight;

	    @FXML
	    private TextField txt_interventions;

	    @FXML
	    private TextField txt_ownerN;

	    @FXML
	    private TextField txt_ownerP;

	    @FXML
	    private TextField txt_ownerE;
	    
	    @FXML
	    private Label lbl_status;
	    
	    public void Add(ActionEvent event) throws Exception {
	    	
	    	String name = txt_nameA.getText();
	    	String species = txt_species.getText();
	    	String weight = txt_weight.getText();
	    	String birth = txt_dateBirth.getText();
	    	String intervention = txt_interventions.getText();
	    	String ownerName = txt_ownerN.getText();
	    	String ownerPhone = txt_ownerP.getText();
	    	String ownerEmail = txt_ownerE.getText();    	
	    	
	    	Animal animalx = new Animal();
	    	Date dateB = new SimpleDateFormat("dd/MM/yyyy").parse(birth);
	    	//animalx.createAnimal(6,name,species,dateB, Byte.valueOf(intervention), ownerName, ownerPhone);
	    	lbl_status.setText("Animal added !");
		}
}
