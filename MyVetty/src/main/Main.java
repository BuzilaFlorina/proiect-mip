package main;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import map.AppLocation;
import model.Animal;
import model.Appointment;
import model.MedicalPersonnel;
import util.DatabaseUtil;
import generic.Cat;
import generic.Dog;
import generic.Owner;

public class Main extends Application {

	private static Map<Integer, AppLocation> locationsApp = new HashMap<>();

	public static void main(String[] args) throws Exception {
		DatabaseUtil dbUtil = new DatabaseUtil();
		
		Scanner in = new Scanner(System.in);
		
		dbUtil.setUp();
		dbUtil.startTransaction();
		
		int choice;
		System.out.println("What do you want to do ?\n");
		System.out.println("0 - create a new animal \n1 - delete an animal \n2 - update an animal \n3 - print all animals from the database \n"
			+ "4 - create a new medical personnel \n5 - delete a medical personnel \n6 - update a medical personnel \n7 - print all medicals personnel \n"
				+ "8 - create a new appointment \n9 - sort the appointments \n10 - update an appointment \n11 - delete an appointment \n "
				+ "12 - print all the appointments from the database \n13 - just a comparator \n14 - Lambda \n15 - Generic \n16 - Map \n Enter the application otherwise \n");
		choice = in.nextInt();
		
		System.out.println();
		
		switch(choice)
		{
		case 0:{
			Animal animalX = new Animal();
			animalX.createAnimal(dbUtil);
			break;
		}
		case 1: {
			System.out.println("Which animal do you want to delete ? Write the id :\n");
			int id = in.nextInt();
			dbUtil.removeAnimal(id);
			break;
		}
		case 2 : {
			System.out.println("Write the id of the animal you want to update :\n");
			int id = in.nextInt();
			System.out.println("The name : \n");
			String name = in.next();
			System.out.println("The species : \n");
			String species = in.next();
			System.out.println("The weight : \n");
			int weight = in.nextInt();
			System.out.println("The date when it was born : \n");
			String date = in.next();
			System.out.println("Did the animal have any preinterventions ? 1-Yes, 0-No");
			int pre = in.nextInt();
			System.out.println("The name of the owner :\n");
			String nameO = in.next();
			System.out.println("The phone of the owner : \n");
			String phoneO = in.next();
			System.out.println("The email of the owner : \n");
			String emailO = in.next();
			
			dbUtil.updateAnimal(id, name, species, date, (byte) pre, weight, phoneO, nameO, emailO);
			break;
		}
		case 3 : {
			System.out.println("The animals are :\n");
			dbUtil.printAllAnimalsFromDB();
			break;
		}
		
		case 4 : {
			MedicalPersonnel medi = new MedicalPersonnel();
			medi.createMedicalPersonnel(dbUtil);
			break;
		}
		case 5 : {
			
			System.out.println("Which medical personnel do you want to delete ? Write the id :\n");
			int id = in.nextInt();
			dbUtil.removeMedicalPersonnel(id);
			break;
		}
		
		case 6 : {
			System.out.println("Write the id of the medical personnel you want to update :\n");
			int id = in.nextInt();
			System.out.println("The new firstname : \n");
			String firstname = in.next();
			System.out.println("The new lastname : \n");
			String lastname = in.next();
			System.out.println("The new age : \n");
			int age = in.nextInt();
			System.out.println("The new date when the medical personnel started working here :\n");
			String dataS = in.next();
			System.out.println("The new date when the medical personnel stopped working here :\n");
			String dataE = in.next();
			dbUtil.updateMedicalPersonnel(id, firstname, lastname, age, dataS, dataE);
			break;
		}
		case 7 : {
			System.out.println("Here are all the medical pesonnel from the database :\n");
			dbUtil.printAllMedicsFromDB();
			break;
		}
		case 8 : {
			Appointment appointment = new Appointment();
			appointment.createAppointment(dbUtil);
			break;
		}
		case 9 : {		
			Appointment appointment = new Appointment();
			appointment.sortAppointmets(dbUtil);
			System.out.println("Appointments sorted !\n");
			break;
		}
		
		case 10 : {
			System.out.println("The id of the appointment that you want to update :\n");
			int id = in.nextInt();
			System.out.println("The new date :\n");
			String date = in.next();
			dbUtil.updateAppointment(id, date);
		}
		
		case 11 : {
			System.out.println("Write the id of the appointment you want to delete : \n");
			int id = in.nextInt();
			 dbUtil.removeAppointment(id);
		}
		
		case 12 : {
			System.out.println( "All appointments from the database : \n");
			dbUtil.printAllAppointmentsFromDB();
		}
		
		case 13 : {
			System.out.println("Comparator");
			List<Animal> animals = new ArrayList();
			animals = dbUtil.animalList();
			Comparator<Animal> comparatorNames = (ani1, ani2) -> { return
			ani1.getName().compareTo(ani2.getName()); };
			Collections.sort(animals, comparatorNames);
		}
		
		case 14 : {
			System.out.println("Lambda");
			
			List<Animal> animals = dbUtil.animalList();
			List<Appointment> apps = dbUtil.appList();
	
			// Sort the names of the animals in order
			Collections.sort(animals, (ani1, ani2) -> ani1.getName().compareTo(ani2.getName())); // lambda expression
	
			System.out.println("The animals sorted by name :");
			for (Animal ani : animals) {
				System.out.println(ani.getName());
			}
	
			// Sort the animals by the date of birth
			Collections.sort(animals, (ani1, ani2) -> ani1.getDateOfBirth().compareTo(ani2.getDateOfBirth()));
	
			System.out.println("\nThe animals sorted by date of birth :");
			for (Animal ani : animals) {
				System.out.println(ani.getName() + " " + ani.getDateOfBirth());
			}
	
			// Shows the name of the animal and the name of the owner with upper cases.
			AnimalOwnerUpperConcat ao = (nameAnimal, nameOwner) -> nameAnimal + " + " + nameOwner.toUpperCase();
	
			System.out.println("\nThe animals + their owners :");
			for (Animal anii : animals) {
				String sillyString = AnimalOwner(ao, anii.getName(), anii.getOwnerName());
				System.out.println(sillyString);
			}
	
			// Shows the name of the animal and the date of the appointment
			DateAppointmentAnimal aa = (nameAnimal, date) -> nameAnimal + " - " + date;
	
			System.out.println("\nThe name of the animal and the date of the appointment :");
			for (Appointment app : apps) {
				String result = AppAnimal(aa, app.getAnimalBean().getName(), app.getDate());
				System.out.println(result);
			}
			
		}
		case 15 : {
			System.out.println("Generic : \n");
			
			// a list with the animals that have the species dog
			List<Animal> dogsAnimals = dbUtil.dogList();
	
			// a list with the animals that have the species cat
			List<Animal> catsAnimals = dbUtil.catList();
	
			List<Dog> dogs = new ArrayList();
	
			// i added the names of the animals with the species dogs in the new list
			for (Animal doggo : dogsAnimals) {
				dogs.add(new Dog(doggo.getName()));
			}
	
			System.out.println("\nNumber of dogs " + dogs.size());
	
			// i added the names of the animals with the species cat in the new list
			List<Cat> cats = new ArrayList();
	
			for (Animal gata : catsAnimals) {
				cats.add(new Cat(gata.getName()));
			}
	
			System.out.println("\nNumber of cats " + cats.size());
	
			// i made a list with the cat owners
			List<String> catOwners = new ArrayList();
			for (Animal cat : catsAnimals) {
				catOwners.add(cat.getOwnerName());
			}
	
			// a list with the dog owners
			List<String> dogOwners = new ArrayList();
			for (Animal dog : dogsAnimals) {
				dogOwners.add(dog.getOwnerName());
			}
	
			// i added every animal that first dog owner has
			Owner<Cat> owner1 = new Owner(dogOwners.get(0), 3, 3);
			for (Dog dogOwner : dogs) {
				if (dogOwner.getName() == owner1.getName())
					owner1.addMammal(dogOwner);
			}
	
			// i added every animal that secound dog owner has
			Owner<Dog> owner2 = new Owner(dogOwners.get(1), 1, 4);
			for (Dog dogOwner : dogs) {
				if (dogOwner.getName() == owner2.getName())
					owner2.addMammal(dogOwner);
			}
	
			// i added every animal that first cat owner has
			Owner<Dog> owner3 = new Owner(catOwners.get(0), 2, 3);
			for (Cat catOwner : cats) {
				if (catOwner.getName() == owner3.getName())
					owner3.addMammal(catOwner);
			}
	
			System.out.println();
			owner1.treatedAnimals();
	
			System.out.println();
			owner2.treatedAnimals();
	
			System.out.println();
			owner3.treatedAnimals();
	
			if (owner2.compareTo(owner3) == -1)
				System.out.println(owner2.getName() + " has more appointments than " + owner3.getName());
			else if (owner2.compareTo(owner3) == 1)
				System.out.println(owner3.getName() + " has more appointments than " + owner2.getName());
			else
				System.out.println(owner2.getName() + " has the same number of appointments with " + owner3.getName());
	
			System.out.println();
		}
		case 16 : {
			
			System.out.println("The map : \n");

					Map<Integer, String> availableLocations = new HashMap<>();
					availableLocations.put(0, "On the moon");
					availableLocations.put(1, "Long Street, number 99");
					availableLocations.put(2, "In the tree");
					availableLocations.put(3, "In Paris");
					availableLocations.put(4, "On my mind");
			
					List<Appointment> appList = dbUtil.appList();
			
					//for each appointment I introduced a location
					for (Appointment app : appList) {
						//while i introduced a correct key for the available locations
						while (true) {
							Scanner in1 = new Scanner(System.in);
							System.out.println("These are the available locations :");
							//i printed the available locations
							Set<Entry<Integer, String>> hashSet = availableLocations.entrySet();
							for (Entry entry : hashSet) {
			
								System.out.println("Key: " + entry.getKey() + ", Location: " + entry.getValue().toString());
							}
							System.out.println();
							System.out.println(
									"What location should we put for the appointment with the id " + app.getIdappointment() + " ?");
							int key = in1.nextInt();
							//i checked if the read key is correct
							if (availableLocations.containsKey(key)) {
								locationsApp.put(app.getIdappointment(),
										new AppLocation(app.getIdappointment(), availableLocations.get(key)));
								availableLocations.remove(key);
								break;
							} else
								System.out.println("You introduced a wrong key! Try again.");
							System.out.println();
						}
						
					}

					Set<Entry<Integer, AppLocation>> hashSet = locationsApp.entrySet();
					for (Entry entry : hashSet) {
			
						System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue().toString());
					}
					
					for(AppLocation app : locationsApp.values())
					{
						System.out.println(app.toString());
					}
					break;
		}
		default : break;
		
		}

		dbUtil.commitTransaction();
		dbUtil.closeEntityManager();
		launch(args);
	}
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		try {
			Pane root = (Pane) FXMLLoader.load(getClass().getResource("/controllers/Login.fxml"));
			Scene scene = new Scene(root, 600, 400);
			primaryStage.setScene(scene);
			primaryStage.show();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public final static String AnimalOwner(AnimalOwnerUpperConcat ao, String nameAnimal, String nameOwner) {
		return ao.animalOwnerUpperConcat(nameAnimal, nameOwner);
	}

	public final static String AppAnimal(DateAppointmentAnimal aa, String animalName, Date data) {
		return aa.dateAppointmentAnimal(animalName, data);
	}

}

interface AnimalOwnerUpperConcat {

	public String animalOwnerUpperConcat(String nameAnimal, String nameOwner);
}

interface DateAppointmentAnimal {

	public String dateAppointmentAnimal(String nameAnimal, Date date);
}