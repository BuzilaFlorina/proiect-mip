package Tests;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Test;

import model.Animal;
import model.Appointment;
import model.Diagnostic;
import util.DatabaseUtil;

class ModelTests {

	/**
	 * this is a test for the animal constructor with parameters
	 * this should create a new animal with the given dates
	 */
	@Test
	void animalConstructorWithParamaeters() {
		Animal ani = new Animal(1, "Violeta", "chicken", 8 , "Loco", "07229281", "lococioco@yahso.ro", (byte) 1);
		
		if(!(ani.getIdanimal() == 1 && ani.getName().equals("Violeta") && ani.getSpecies().equals("chicken") && ani.getWeight() == 8 && ani.getOwnerName().equals("Loco")))
			fail();
	}
	
	/**
	 * this is a test for the diagnostic constructor with parameters
	 * this should create a new diagnostic with the given values
	 */
	@Test
	void diagnosticConstructorWithParameters() {
		Diagnostic diag = new Diagnostic (0, "Picior", "-nu are picior", 1, 2, "15/01/2019");
		
		assertEquals(diag.getIddiagnostic(), 0);
		assertEquals(diag.getDescription(), "-nu are picior");
		assertEquals(diag.getName(), "Picior");
		assertEquals(diag.getAnimal(), 2);
		//assertEquals(diag.getMedicalpersonnel(), 2);
	}
	
	/**
	 * this is a test of the sortedAppointments function
	 * the function return a list of appointments sorted by date
	 * @throws Exception
	 */
	@Test
	void sortedApppointmentsTest() throws Exception {
		
		List<Appointment> apps = new ArrayList();
		
		Appointment app1 = new Appointment();
		Appointment app2 = new Appointment();
		Appointment app3 = new Appointment();
		Appointment app4 = new Appointment();
		
		Date data = new SimpleDateFormat("dd/MM/yyyy").parse("20/10/2019");
		app1.setDate(data);
		Date data2 = new SimpleDateFormat("dd/MM/yyyy").parse("20/11/2019");
		app2.setDate(data);
		Date data3 = new SimpleDateFormat("dd/MM/yyyy").parse("21/11/2019");
		app3.setDate(data);
		Date data4 = new SimpleDateFormat("dd/MM/yyyy").parse("22/12/2020");
		app4.setDate(data);
		
		apps.add(app4);
		apps.add(app2);
		apps.add(app3);
		apps.add(app1);
		
		List<Appointment> appsSortedByDate = new ArrayList();
		
	    appsSortedByDate = (List<Appointment>) app1.sortedApps(apps);
	    
	    assertEquals(apps.get(0), appsSortedByDate.get(0));
	    assertEquals(apps.get(1), appsSortedByDate.get(1));
	    assertEquals(apps.get(2), appsSortedByDate.get(2));
	    assertEquals(apps.get(3), appsSortedByDate.get(3));
	    
		
	}
}
