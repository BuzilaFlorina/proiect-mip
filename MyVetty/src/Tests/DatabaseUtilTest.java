package Tests;

import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import model.Animal;
import util.DatabaseUtil;

import org.eclipse.persistence.jpa.jpql.Assert;
import org.junit.jupiter.api.Test;

class DatabaseUtilTest {

	/**
	 * test for the updatedAnimal method that should change the dates of an animal
	 * @throws Exception
	 */
	@Test
	void updateAnimaltest() throws Exception {
		DatabaseUtil db = new DatabaseUtil();
		db.setUp();
		db.startTransaction();
		
		Animal ani = new Animal();
		ani.setIdanimal(1);
		ani.setName("Lolo");
		ani.setSpecies("dog");
		ani.setPreInterventions((byte) 0);
		String date = "20/11/2019";
		Date dataB = new SimpleDateFormat("dd/MM/yyyy").parse(date);
		ani.setDateOfBirth(dataB);
		
		db.commitTransaction();
		db.saveAnimal(ani);
		
		db.updateAnimal(1, "Lala", "cat", "20/10/1998", (byte) 1, 5, "029283", "Mirel", "fefe@yahoo.com");
		
		db.closeEntityManager();
		
		assertEquals(ani.getSpecies(), "cat");
		assertEquals(ani.getName(), "Lala");
	}
	
	/**
	 * test for createAnimal method that should create a new animal in the database
	 * @throws Exception
	 */
	@Test
	void createAnimalTest() throws Exception {
		
		Animal ani = new Animal();
		ani.crtAnimal(110, "Liliac", "liliac", "20/10/2010", (byte) 0, "Liliachiu", "08777280982", "liliac.ma.fac@lili.ro");
		
		assertEquals(ani.getIdanimal(), 10);
		assertEquals(ani.getName(), "Liliac");
		assertEquals(ani.getSpecies(), "liliac");
		assertEquals(ani.getOwnerName(), "Liliachiu");
	}
	
	/**
	 * test for removeAnimal method that should delete an animal from the database
	 * @throws Exception
	 */
	@Test
	void removeAnimalTest() throws Exception {
		
		DatabaseUtil db = new DatabaseUtil();
		db.setUp();
		db.startTransaction();
		
		Animal ani = new Animal();
		ani.setIdanimal(1);
		ani.setName("Lolo");
		ani.setSpecies("dog");
		ani.setPreInterventions((byte) 0);
		String date = "20/11/2019";
		Date dataB = new SimpleDateFormat("dd/MM/yyyy").parse(date);
		ani.setDateOfBirth(dataB);
		
		db.commitTransaction();
		db.saveAnimal(ani);
		
		db.removeAnimal(1);
		
		Assert.isTrue(db.animalList().contains(ani) == false, "Not nice");
		
		db.closeEntityManager();
	}
}
