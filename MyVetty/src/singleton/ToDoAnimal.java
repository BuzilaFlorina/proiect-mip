package singleton;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import javafx.collections.FXCollections;

public class ToDoAnimal {

	private static ToDoAnimal instance = new ToDoAnimal();

	private final String fillename = "Diagnostic.txt";

	private List<Diagnostic> diagnostics;

	public static ToDoAnimal getInstance() {
		return instance;
	}

	public static void setInstance(ToDoAnimal instance) {
		ToDoAnimal.instance = instance;
	}

	public void setPrescriptions(List<Diagnostic> prescriptions) {
		this.diagnostics = prescriptions;
	}

	public List<Diagnostic> getPrescription() {
		return diagnostics;
	}

	public String getFillename() {
		return fillename;
	}
	
	/**
	 * this function reads from a file the diagnostics
	 * @throws IOException
	 */
	public void loadPrescriptions() throws IOException {

		Path path = Paths.get(fillename);
		diagnostics = FXCollections.observableArrayList();

		BufferedReader br = Files.newBufferedReader(path);
		String input;
		try {
			while ((input = br.readLine()) != null) {

				String[] prescriptionPieces = input.split("\t");
				String description = prescriptionPieces[0];
				String idAnimal = prescriptionPieces[1];
				br.readLine();
				Diagnostic diagnostic = new Diagnostic(description, idAnimal);
				diagnostics.add(diagnostic);
			}
		} finally {
			if (br != null)
				br.close();
		}

	}

}
