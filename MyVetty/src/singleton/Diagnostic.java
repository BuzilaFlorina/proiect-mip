package singleton;

public class Diagnostic{

	private String description;
	private String idAnimal;
	
	
	/**
	 * this is the constructor for the class Diagnostic
	 * @param description is the description of the diagnostic
	 * @param idAnimal is the animal that have the diagnostic given
	 */
	public Diagnostic(String description, String idAnimal) {
		super();
		this.description = description;
		this.idAnimal = idAnimal;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getIdAnimal() {
		return idAnimal;
	}
	
	public void setIdAnimal(String id) {
		this.idAnimal = id;
	} 

	public void setAppointmentDate(String idAnimal) {
		this.idAnimal = idAnimal;
	}

}
