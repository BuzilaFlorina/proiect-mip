package map;

import java.util.HashMap;
import java.util.Map;

public class AppLocation {

	private final int id;
	private final String location;
	private final Map<String, Integer> apps;

	/**
	 * a constructor of the AppLocation class
	 * @param locationID is an identifier of the location
	 * @param location is the name of the location
	 */
	public AppLocation(int locationID, String location) {
		this.id = locationID;
		this.location = location;
		this.apps = new HashMap<String, Integer>();
	}

	public void addLocation(String location, int id) {
		apps.put(location, id);
	}

	public int getId() {
		return this.id;
	}

	public String getLocation() {
		return location;
	}

}
