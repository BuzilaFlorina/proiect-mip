package generic;

public class Mammal {

	private String _name;	
	
	public Mammal (String name){
		this._name = name;
	}
	
	public String getName(){
		return this._name;
	}
}
