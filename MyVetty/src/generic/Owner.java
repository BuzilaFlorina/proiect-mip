package generic;

import util.DatabaseUtil;
import java.util.ArrayList;

public class Owner<T extends Mammal> implements Comparable<Owner<T>> {

	private String _name;
	int nrTreatedAnimals = 0;
	private int numAppointments = 0;
	int numAppDone = 0;

	private ArrayList animals = new ArrayList<>();

	/**
	 * a constructor of an owner
	 * @param name is the name of the owner
	 * @param numAppDone is the number of the appointments done 
	 * @param numApp is the number of all the appointments that the owner must do
	 */
	public Owner(String name, int numAppDone, int numApp) {
		this._name = name;
		this.numAppDone = numAppDone;
		this.numAppointments = numApp;
	}

	public String getName() {
		return _name;
	}

	public void setnumAppointments(int number) {
		this.numAppointments = number;
	}

	public int getnumAppointments() {
		return numAppointments;
	}

	public int getNrTreatedAnimals() {
		return nrTreatedAnimals;
	}

	public int getNumAppDone() {
		return numAppDone;
	}

	public int getNumAppointments() {
		return numAppointments;
	}

	public void setName(String name) {
		this._name = name;
	}

	/**
	 * this functions adds a mammal in an arraylist animals
	 * @param mammal is the mammal that will be added
	 * @return
	 */
	public boolean addMammal(Mammal mammal) {

		if (animals.contains(mammal)) {
			System.out.println("The animal " + mammal.getName() + " already exists.");
			return false;
		} else {
			animals.add(mammal);
			System.out.println(mammal.getName() + " was added.");
			return true;
		}

	}

	public int numAnimals() {
		return this.animals.size();
	}

	/**
	 * checks if the owner has any appointments left
	 * if the owner has finished his appointments, number of treated animals will increase
	 * otherwise number of appointments done by the owner will be increasing
	 */
	public void treatedAnimals() {
		String message;

		if (numAppDone == numAppointments) {
			nrTreatedAnimals++;
			message = "We treated "+ nrTreatedAnimals + " animlas from " + this.getName();
		} else {
			numAppDone++;
			int numOfAppLeft = numAppointments - numAppDone;
			if (numAppDone == numAppointments) {
				message = this.getName() + ", we have finished our appointments.";
			} else {
				message = this.getName() + ", you have " + numOfAppLeft + " appointments left. See you again !";
			}
		}

		System.out.println(message);

	}

	/**
	 * checks who has more appointments
	 */
	@Override
	public int compareTo(Owner<T> owner) {
		if (this.numAppointments < owner.numAppointments) {
			return 1;
		} else if (this.numAppointments > owner.numAppointments) {
			return -1;
		}
		return 0;
	}

}
