package model;

import java.io.Serializable;
import java.text.SimpleDateFormat;

import javax.persistence.*;

import util.DatabaseUtil;

import java.util.Date;
import java.util.List;
import java.util.Scanner;

/**
 * The persistent class for the medical_personnel database table.
 * 
 */
@Entity
@Table(name = "medical_personnel")
@NamedQuery(name = "MedicalPersonnel.findAll", query = "SELECT m FROM MedicalPersonnel m")
public class MedicalPersonnel implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int idPersonnel;

	private int age;

	@Temporal(TemporalType.DATE)
	@Column(name = "end_date")
	private Date endDate;

	private String firstname;

	private String lastname;

	@Temporal(TemporalType.DATE)
	@Column(name = "start_date")
	private Date startDate;

	// bi-directional many-to-one association to Appointment
	@OneToMany(mappedBy = "medicalPersonnel")
	private List<Appointment> appointments;

	public MedicalPersonnel() {
	}

	public int getIdPersonnel() {
		return this.idPersonnel;
	}

	public void setIdPersonnel(int idPersonnel) {
		this.idPersonnel = idPersonnel;
	}

	public int getAge() {
		return this.age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public Date getEndDate() {
		return this.endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getFirstname() {
		return this.firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return this.lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public Date getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public List<Appointment> getAppointments() {
		return this.appointments;
	}

	public void setAppointments(List<Appointment> appointments) {
		this.appointments = appointments;
	}

	/**
	 * this functions adds an appointment 
	 * @param appointment is the appointment to be added
	 * @return
	 */
	public Appointment addAppointment(Appointment appointment) {
		getAppointments().add(appointment);
		appointment.setMedicalPersonnel(this);

		return appointment;
	}

	/**
	 * this functions removes an appointment
	 * @param appointment is the appointment that will be removed
	 * @return
	 */
	public Appointment removeAppointment(Appointment appointment) {
		getAppointments().remove(appointment);
		appointment.setMedicalPersonnel(null);

		return appointment;
	}

	/**
	 * this function is used to create a medical personnel
	 * @param dbUtil is the database where the medical personnel will be saved
	 * @throws Exception
	 */
	public void createMedicalPersonnel(DatabaseUtil dbUtil) throws Exception {

		MedicalPersonnel medic = new MedicalPersonnel();
		Scanner in = new Scanner(System.in);

		System.out.println("Write down the id of the doctor\n");
		int id = in.nextInt();
		medic.setIdPersonnel(id);

		System.out.println("Write down the firstname of the doctor\n");
		String firstname = in.next();
		medic.setFirstname(firstname);

		System.out.println("Write down the lastname of the doctor\n");
		String lastname = in.next();
		medic.setLastname(lastname);

		System.out.println("Write down the date when the doctor started working\n");
		String dateStart = in.next();

		Date dataS = new SimpleDateFormat("dd/MM/yyyy").parse(dateStart);
		medic.setStartDate(dataS);

		System.out.println("Write down the date when the doctor stopped working\n");
		String dateEnd = in.next();

		Date dataE = new SimpleDateFormat("dd/MM/yyyy").parse(dateEnd);
		medic.setEndDate(dataE);

		dbUtil.setUp();
		dbUtil.startTransaction();
		dbUtil.saveMedic(medic);
		dbUtil.commitTransaction();
		in.close();
	}

}