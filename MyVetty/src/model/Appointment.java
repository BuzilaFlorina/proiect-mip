package model;

import java.io.Serializable;
import java.text.SimpleDateFormat;

import javax.persistence.*;

import util.DatabaseUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;


/**
 * The persistent class for the appointment database table.
 * 
 */
@Entity
@NamedQuery(name="Appointment.findAll", query="SELECT a FROM Appointment a")
public class Appointment implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idappointment;

	@Temporal(TemporalType.DATE)
	private Date date;

	//bi-directional many-to-one association to Animal
	@ManyToOne
	@JoinColumn(name="animal")
	private Animal animalBean;

	//bi-directional many-to-one association to MedicalPersonnel
	@ManyToOne
	@JoinColumn(name="doctor")
	private MedicalPersonnel medicalPersonnel;

	public Appointment() {
	}

	public int getIdappointment() {
		return this.idappointment;
	}

	public void setIdappointment(int idappointment) {
		this.idappointment = idappointment;
	}

	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Animal getAnimalBean() {
		return this.animalBean;
	}

	public void setAnimalBean(Animal animalBean) {
		this.animalBean = animalBean;
	}

	public MedicalPersonnel getMedicalPersonnel() {
		return this.medicalPersonnel;
	}

	public void setMedicalPersonnel(MedicalPersonnel medicalPersonnel) {
		this.medicalPersonnel = medicalPersonnel;
	}

	/**
	 * this function creates a new appointment in the database
	 * @param dbUtil is the database where the appointment will be saved
	 * @throws Exception
	 */
	public void createAppointment(DatabaseUtil dbUtil) throws Exception
	{
		Appointment app = new Appointment();
		Scanner in = new Scanner(System.in);
		System.out.println("Write the id of the appointment");
		int id = in.nextInt();
		
		dbUtil.setUp();
		dbUtil.startTransaction();
		
		app.setIdappointment(id);
		
		System.out.println("Write down the date of your appointment\n");
		String data = in.next();
		Date date = new SimpleDateFormat("dd/MM/yyyy").parse(data);
		app.setDate(date);
			
		System.out.println("Write the id of your animal:");
	    int idAni = in.nextInt();
	    if(dbUtil.findAnimal(idAni) != null)
	    {
	    app.setAnimalBean(dbUtil.findAnimal(idAni));
	    }
	    
	    System.out.println("Write the id of your medic:");
	    int idMedic = in.nextInt();
	      
	    if(dbUtil.findDoctor(idMedic) != null)
	    {
	    app.setMedicalPersonnel(dbUtil.findDoctor(idMedic));
	    }
	    
	    
		dbUtil.saveAppointment(app);
		dbUtil.commitTransaction();
		in.close();
	}

	/**
	 * this functions sorts the appointments by dates, desc
	 * @param db is the database where the appointments are located
	 */
	public void sortAppointmets (DatabaseUtil db)
	{
		List<Appointment> results = db.entityManager.createNativeQuery("Select * From my_vetty.Appointment", Appointment.class).getResultList();
		
		for(Appointment app : results)
			for(Appointment app2 : results)
			{
				if(app.getDate().compareTo(app2.getDate()) > 0)
				{
					 Date swapDate1 = app.getDate();
					 Date swapDate2 = app2.getDate();
					 app2.setDate(swapDate1);
					 app.setDate(swapDate2);
				}

			}
		for(Appointment app : results)
			System.out.println("The appointment with the id " + app.getIdappointment()+ " is on the date :" + app.getDate() + " and is with the doctor " + app.getMedicalPersonnel().getFirstname() + " and the animal " + app.getAnimalBean().getName());
	}

	/**
	 * this functions sorts the appointments by dates, asc
	 * @param db is the database where the appointments are located
	 * @return a list with the sorted appointments
	 */
	public List<Appointment> sortedApp(DatabaseUtil db) {
		List<Appointment> results = db.entityManager.createNativeQuery("Select * From my_vetty.Appointment", Appointment.class).getResultList();
		
		for(Appointment app : results)
			for(Appointment app2 : results)
			{
				if(app.getDate().compareTo(app2.getDate()) < 0)
				{
					 Date swapDate1 = app.getDate();
					 Date swapDate2 = app2.getDate();
					 app2.setDate(swapDate1);
					 app.setDate(swapDate2);
				}

			}
		return results;
	}
	
	/**
	 * this functions sorts the appointments by dates, desc
	 * @param results is the list that will be sorted
	 * @return
	 */
	public List<Appointment> sortedApps(List<Appointment> results) {
		
		for(Appointment app : results)
			for(Appointment app2 : results)
			{
				if(app.getDate().compareTo(app2.getDate()) < 0)
				{
					 Date swapDate1 = app.getDate();
					 Date swapDate2 = app2.getDate();
					 app2.setDate(swapDate1);
					 app.setDate(swapDate2);
				}

			}
		return results;
	}
	
}