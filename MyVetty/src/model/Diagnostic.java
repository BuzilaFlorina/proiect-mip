package model;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the diagnostic database table.
 * 
 */
@Entity
@NamedQuery(name="Diagnostic.findAll", query="SELECT d FROM Diagnostic d")
public class Diagnostic implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int iddiagnostic;

	private int animal;

	@Temporal(TemporalType.DATE)
	private Date date;

	@Lob
	private String description;

	private int medicalpersonnel;

	private String name;

	public Diagnostic() {
	}

	/**
	 * this is the constructor with parameters of the class Diagnostic
	 * @param id is the identifier of the diagnostic
	 * @param name is the name of the diagnostic
	 * @param descr is the description of the diagnostic
	 * @param medicalpersonnel is the medical personnel that wrote the diagnostic
	 * @param animal is the animal that has that diagnostic
	 * @param data is the date when was written
	 */
	public Diagnostic(int id, String name, String descr, int medicalpersonnel, int animal, String data) {
		this.iddiagnostic = id;
		this.name = name;
		this.description = descr;
		this.medicalpersonnel = medicalpersonnel;
		this.animal = animal;		
		Date dataB = null;
		try {
			dataB = new SimpleDateFormat("dd/MM/yyyy").parse(data);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.setDate(dataB);
	}

	public int getIddiagnostic() {
		return this.iddiagnostic;
	}

	public void setIddiagnostic(int iddiagnostic) {
		this.iddiagnostic = iddiagnostic;
	}

	public int getAnimal() {
		return this.animal;
	}

	public void setAnimal(int animal) {
		this.animal = animal;
	}

	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getMedicalpersonnel() {
		return this.medicalpersonnel;
	}

	public void setMedicalpersonnel(int medicalpersonnel) {
		this.medicalpersonnel = medicalpersonnel;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}