package model;

import java.io.Serializable;
import java.text.SimpleDateFormat;

import javax.persistence.*;

import util.DatabaseUtil;

import java.util.Date;
import java.util.List;
import java.util.Scanner;


/**
 * The persistent class for the animal database table.
 * 
 */
@Entity
@NamedQuery(name="Animal.findAll", query="SELECT a FROM Animal a")
public class Animal implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idanimal;

	@Temporal(TemporalType.DATE)
	private Date dateOfBirth;

	private String name;

	private String ownerName;

	private String ownerPhone;
	
	private String ownerEmail;

	@Column(name="pre_interventions")
	private byte preInterventions;

	private String species;

	private int weight;
	
	@Lob
	private byte[] animalPicture;
	
	public byte[] getAnimalPicture() {
		return this.animalPicture;
	}

	public void setAnimalPicture(byte[] animalPicture) {
		this.animalPicture = animalPicture;
	}

	//bi-directional many-to-one association to Appointment
	@OneToMany(mappedBy="animalBean")
	private List<Appointment> appointments;

	public Animal() {
	}
	
	/**
	 * is a constructor of the class Animal with parameters
	 * @param id is the identifier of the animal
	 * @param name is the name of the animal
	 * @param species is the species of the animal
	 * @param weight is the weight of the animal (in kg)
	 * @param nameOwner is the name of the owner of the animal
	 * @param ownerPhone is the phone of the owner of the animal
	 * @param ownerEmail is the email of the owner of the animal
	 * @param interventions is 0 if the animal didn't have any interventons before, or 1 otherwise
	 */
	public Animal(int id, String name, String species, int weight, String nameOwner, String ownerPhone, String ownerEmail, byte interventions)
	{
		this.setIdanimal(id);
		this.setName(name);
		this.setOwnerName(nameOwner);
		this.setSpecies(species);
		this.setWeight(weight);
		this.setPreInterventions(interventions);
		this.setOwnerPhone(ownerPhone);
		this.setOwnerEmail(ownerEmail);
	}

	public int getIdanimal() {
		return this.idanimal;
	}

	public void setIdanimal(int idanimal) {
		this.idanimal = idanimal;
	}

	public Date getDateOfBirth() {
		return this.dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getOwnerEmail() {
		return this.ownerEmail;
	}

	public void setOwnerEmail(String ownerEmail) {
		this.ownerEmail = ownerEmail;
	}

	public String getOwnerName() {
		return this.ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public String getOwnerPhone() {
		return this.ownerPhone;
	}

	public void setOwnerPhone(String ownerPhone) {
		this.ownerPhone = ownerPhone;
	}

	public byte getPreInterventions() {
		return this.preInterventions;
	}

	public void setPreInterventions(byte preInterventions) {
		this.preInterventions = preInterventions;
	}

	public String getSpecies() {
		return this.species;
	}

	public void setSpecies(String species) {
		this.species = species;
	}

	public int getWeight() {
		return this.weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public List<Appointment> getAppointments() {
		return this.appointments;
	}

	public void setAppointments(List<Appointment> appointments) {
		this.appointments = appointments;
	}

	/**
	 * this functions adds an appointment 
	 * @param appointment is the appointment to be added
	 * @return
	 */
	public Appointment addAppointment(Appointment appointment) {
		getAppointments().add(appointment);
		appointment.setAnimalBean(this);

		return appointment;
	}

	/**
	 * this functions removes an appointment
	 * @param appointment is the appointment that will be removed
	 * @return
	 */
	public Appointment removeAppointment(Appointment appointment) {
		getAppointments().remove(appointment);
		appointment.setAnimalBean(null);

		return appointment;
	}
	
	/**
	 * this function creates a new animal in the databases
	 * @param dbUtil is the database where the animal will be saved
	 * @throws Exception
	 */
	public void createAnimal(DatabaseUtil dbUtil) throws Exception
	{
		Animal animalX = new Animal();
		Scanner in = new Scanner(System.in);
		System.out.println("Write down the id of your animal\n");
		int id = in.nextInt();
		animalX.setIdanimal(id);
		
		System.out.println("Write down the name of your animal\n");
		String name = in.next();
		animalX.setName(name);
		
		System.out.println("Write down the date that your animal was born\n");
		String dateBorn = in.next();
		Date dataB = new SimpleDateFormat("dd/MM/yyyy").parse(dateBorn);
		animalX.setDateOfBirth(dataB);
		
		System.out.println("Write down the species of your animal\n");
		String species = in.next();
		animalX.setSpecies(species);
		
		System.out.println("Write down if your animal had any previous interventions (0-No,1-Yes)\n");
		byte intervention = in.nextByte();
		animalX.setPreInterventions(intervention);
		
		System.out.println("Write the weight of your animal \n");
		int weight = in.nextInt();
		animalX.setWeight(weight);
		
		System.out.println("The name of the owner: \n");
		String nameOwner = in.next();
		animalX.setOwnerName(nameOwner);
		
		System.out.println("The phone of the owner: \n");
		String phoneOwner = in.next();
		animalX.setOwnerPhone(phoneOwner);
		
		System.out.println("The email of the owner: \n");
		String ownerEmail = in.next();
		animalX.setOwnerEmail(ownerEmail);
		
		dbUtil.setUp();
		dbUtil.startTransaction();
		dbUtil.saveAnimal(animalX);
		dbUtil.commitTransaction();
		in.close();
	}
	
	/**
	 * this function creates a new animal in the database
	 * @param id must be unique, it will be used to indentify the animal
	 * @param name of the animal
	 * @param species of the animal
	 * @param dateB is when the animal was born
	 * @param intervention says if the animal had some injuries or diseases before
	 * @param nameOwner the name of the Owner of the animal
	 * @param phoneOwner the phone of the Owner
	 * @throws Exception
	 */
	public void crtAnimal(int id, String name, String species, String dateB, byte intervention, String nameOwner, String phoneOwner, String ownerEmail) throws Exception
	{
		DatabaseUtil dbUtil = new DatabaseUtil();
		
		this.setIdanimal(id);

		this.setName(name);
		
		this.setOwnerEmail(ownerEmail);
		
		Date dataB = new SimpleDateFormat("dd/MM/yyyy").parse(dateB);
		this.setDateOfBirth(dataB);

		this.setSpecies(species);

		this.setPreInterventions(intervention);

		this.setWeight(weight);

		this.setOwnerName(nameOwner);

		this.setOwnerPhone(phoneOwner);
		
		dbUtil.setUp();
		dbUtil.startTransaction();
		dbUtil.saveAnimal(this);
		dbUtil.commitTransaction();
	}
}