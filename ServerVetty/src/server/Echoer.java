package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.HashSet;

public class Echoer extends Thread{
	private Socket socket;

	
	public Echoer(Socket socket)
	{
		this.socket = socket;
	}
	
	public void run() {
		try {
			BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			PrintWriter output = new PrintWriter(socket.getOutputStream(), true);
			
				String echoString = input.readLine();
				String echoString2 = input.readLine();
				
				HashSet<String> usernames = new HashSet<String>();
				usernames.add("medic1");
				usernames.add("medic2");
				usernames.add("medic3");

				HashSet<String> passwords = new HashSet<String>();

				passwords.add("000");
				passwords.add("111");
				passwords.add("222");
				
				if (usernames.contains(echoString) && passwords.contains(echoString2)) {
						output.println("da");
					} else {
						output.println("nu");
					}
			
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				socket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
